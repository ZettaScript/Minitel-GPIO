# Minitel-GPIO

## Wiring

You don't have any old serial port on your computer, or any DIN-5 to USB chinese adapter?
The only component used is a resistor!

Wirings:  
Minitel DIN - Raspberry Pi (BCM)  
1 (RX)      - GPIO 21  
2 (Gnd)     - Gnd  
3 (TX)      - GPIO 20 (with a resistor; works well between 1kOhm-10kOhm)

Reading from Minitel works, sending to Minitel doesn't work completely.
(https://zettascript.org/projects/minitel-gpio)