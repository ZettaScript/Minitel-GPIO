#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright 2017-2018 Pascal Engélibert
Raspberry Pi - Minitel console
See datasheet:
http://pila.fr/content/interface_usb_minitel/specifications%20techniques%20d%27utilisation%20du%20minitel.pdf
"""

from sys import stdout, stdin, stderr
import tty, termios
import minitel_driver as minitel

fdin = stdin.fileno()
old_settings = termios.tcgetattr(fdin)
tty.setraw(fdin)

minitel.initSerial()
loop = True
while loop:
	try:
		for c in stdin.read(1):
			if c == '\x03':
				loop = False
				break
			minitel.sendbuf.databuf.append(ord(c))
	except (UnicodeDecodeError, UnicodeEncodeError):
			stderr.write("En mode tele-informatique, le Minitel ne supporte pas l'ASCII etendu.")
			stderr.flush()

minitel.stop()
termios.tcsetattr(fdin, termios.TCSADRAIN, old_settings)
