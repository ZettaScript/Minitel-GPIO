#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright 2017-2018 Pascal Engélibert
Raspberry Pi - Minitel driver
See datasheet:
http://pila.fr/content/interface_usb_minitel/specifications%20techniques%20d%27utilisation%20du%20minitel.pdf
"""

from time import sleep, time
from threading import Thread, RLock
import RPi.GPIO as gpio
from sys import stdout

FREQ = 1200# frequency (Hz)
PULSE = 1/FREQ# pulse duration (s)
PULSE2 = PULSE/16
PULSE0 = PULSE*10

TX = 21# output pin (RPi out, Minitel in)
RX = 20# input pin (RPi in, Minitel out)

lock = RLock()

def writef(data):
	stdout.write(data)
	stdout.flush()

# outf: output function
class RecBuf(Thread):
	def __init__(self, outf=writef):
		Thread.__init__(self)
		self.work = True
		self.outf = outf
	
	def run(self):
		a = False
		s = []
		outgpio = gpio.output
		lastp = 0
		while self.work:
			if not gpio.input(RX):
				lastp = time()+PULSE0
				s.append(0)
				a = True
			elif time() < lastp:
				s.append(1)
				a = True
			elif a:
				a = False
				n = 0
				v = 0
				o = -1
				c = []
				p = 0
				for b in s:
					if b == v:
						n += 1
					else:
						if n > 0:
							c += [v] * round(n/6)
						v = b
						n = 1
				if n > 0:
					c += [v] * round(n/6)
				while len(c) >= 10 and c[0] == 0 and c[9] == 1:
					if (c[1]+c[2]+c[3]+c[4]+c[5]+c[6]+c[7])&1 == c[8]:
						#stdout.write(chr(c[1]|(c[2]<<1)|(c[3]<<2)|(c[4]<<3)|(c[5]<<4)|(c[6]<<5)|(c[7]<<6)))
						#stdout.write(hex(c[1]|(c[2]<<1)|(c[3]<<2)|(c[4]<<3)|(c[5]<<4)|(c[6]<<5)|(c[7]<<6))+" ")
						#stdout.flush()
						self.outf(chr(c[1]|(c[2]<<1)|(c[3]<<2)|(c[4]<<3)|(c[5]<<4)|(c[6]<<5)|(c[7]<<6)))
						c = c[10:]
				s = []
			sleep(PULSE2)

class SendBuf(Thread):
	def __init__(self):
		Thread.__init__(self)
		self.databuf = []
		self.work = True
	
	def run(self):
		bytebuf = []
		outgpio = gpio.output
		tx_high = False
		LOW = gpio.LOW
		HIGH = gpio.HIGH
		while self.work:
			if len(bytebuf) > 0:
				if bytebuf.pop(0):
					outgpio(TX, HIGH)
					tx_high = True
				else:
					outgpio(TX, LOW)
			if len(bytebuf) == 0 and len(self.databuf) > 0:
				with lock:
					d = self.databuf.pop(0)
				if d != -1:
					#bytebuf = [1,d&0x40,d&0x20,d&0x10,d&0x08,d&0x04,d&0x02,d&0x01, ((d&0x40)+(d&0x20)+(d&0x10)+(d&0x08)+(d&0x04)+(d&0x02)+(d&0x01))&1]
					#bytebuf = [0, d&0x01,d&0x02,d&0x04,d&0x08,d&0x10,d&0x20,d&0x40, ((d&0x40)+(d&0x20)+(d&0x10)+(d&0x08)+(d&0x04)+(d&0x02)+(d&0x01))&1,1]
					bytebuf = [0, d&0x01,d&0x02,d&0x04,d&0x08,d&0x10,d&0x20,d&0x40, 1]
			#sleep(PULSE/2)
			#if tx_high:
			#	outgpio(TX, LOW)
			#	tx_high = False
			#sleep(PULSE/2)
			sleep(PULSE)
	
	# Return false if the buffer is empty
	def busy(self):
		return self.databuf != []

sendbuf = SendBuf()
recbuf = RecBuf()

# Init GPIO and start the com thread
def initSerial(outf=writef):
	recbuf.outf = outf
	gpio.setwarnings(False)
	gpio.setmode(gpio.BCM)
	gpio.setup(TX, gpio.OUT, initial=gpio.HIGH)
	gpio.setup(RX, gpio.IN, pull_up_down=gpio.PUD_UP)
	sendbuf.start()
	recbuf.start()

# Stop com thread
def stop():
	recbuf.work = False
	sendbuf.work = False
	recbuf.join()
	sendbuf.join()
	gpio.cleanup()

busy = sendbuf.busy
