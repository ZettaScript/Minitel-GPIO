#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright 2017-2018 Pascal Engélibert
Raspberry Pi - Minitel FIFO
See datasheet:
http://pila.fr/content/interface_usb_minitel/specifications%20techniques%20d%27utilisation%20du%20minitel.pdf
"""

from sys import stderr
import os, errno
import minitel_driver as minitel

try:
	os.mkfifo("minitel_out")
except FileExistsError:
	True

try:
	os.mkfifo("minitel_in")
except FileExistsError:
	True

BUFFER = 1024
fifo_out = os.open("minitel_out", os.O_RDONLY | os.O_NONBLOCK)
fifo_in = os.open("minitel_in", os.O_WRONLY | os.O_APPEND)

def outf(data):
	os.write(fifo_in, data)

minitel.initSerial(outf)
loop = True
while loop:
	try:
		for c in os.read(fifo_out, BUFFER):
			if c == 3:
				loop = False
				break
			minitel.sendbuf.databuf.append(c)
	except (UnicodeDecodeError, UnicodeEncodeError):
			stderr.write("En mode tele-informatique, le Minitel ne supporte pas l'ASCII etendu.\n")
			stderr.flush()

fifo_out.close()
fifo_in.close()
